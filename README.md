# Unmerge Entity using crosswalk source #

# Description #

This script is used to unmerge entities based on given crosswalk source and merge type 

# How to run script #

* Build project using mvn clean install -U and find jar in target folder or download jar (util-entity-unmerge-crosswalk-source.jar) from {{Project}}/jar/ location


* Usage
```
     nohup java -jar -Xmx2G -Xms2G util-entity-unmerge-crosswalk-source.jar {{properties-file-path}} "{{MERGE_REASON}}" > {output-log-filename} 2>&1 &
```
* Properties file
```
     reltioUrl=https://test-usg.reltio.com
     authUrl=https://auth.reltio.com/oauth/token
     tenant={tenantId}
     sourceSystem=SFDC
     authUser={authUser}
     authUserPwd={authPwd}
     entityType=Account
     readOnly=false
     threadCount=1                  #no of threads to run (MAX_THREADS=500, default=1)
     filePath=<path-to-input-file>  #CSV delimited
```
* Format of comma demilited input file for entities*
```
    entities/1dtJliW6,SFDC
```
* Example:
```
     nohup java -jar -Xmx2G -Xms2G util-entity-unmerge-crosswalk-source.jar unmerge.properties > log_12_09_1.txt 2>&1 &
```
