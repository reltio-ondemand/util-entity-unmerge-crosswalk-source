package com.reltio.scripts.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public class UnmergeEntityFromCrosswalkSource {


    private static final int MAX_THREADS = 500;
    private static final int BATCH_SIZE = 100;
    private static final List<String> REQUIRED_PARAMS = Arrays.asList(
            "tenant",
            "reltioUrl",
            "authUrl",
            "authUser",
            "authUserPwd",
            "filePath",
            "entityType",
            "sourceSystem");

    public static void main(String[] args) throws IOException {

        if (args.length == 0) {
            System.out.println("\nProperty file location is required, Exiting...\n");
            System.exit(1);
        }
        Properties properties = new Properties();
        String propertyFilePath = args[0];
        String mergeReason = args[1] == null ? UnmergeEntityExecutor.DEFAULT_CROSSWALK_MERGE_REASON : args[1].trim();

        try {
            if (!new File(propertyFilePath).exists()) {
                System.out.printf("\nProperty file can't be found in path '%s', Exiting...\n%n", propertyFilePath);
                System.exit(1);
            }
            properties.load(Files.newInputStream(Paths.get(propertyFilePath)));
        } catch (Exception e) {
            System.out.printf("\nFailed to read property file in path '%s', Exiting...\n%n", propertyFilePath);
            System.exit(1);
        }

        String tenantId = properties.getProperty("tenant");
        String reltioApiUrl = properties.getProperty("reltioUrl");
        String reltioAuthUrl = properties.getProperty("authUrl");
        String authUserName = properties.getProperty("authUser");
        String authUserPassword = properties.getProperty("authUserPwd");
        String inputFilePath = properties.getProperty("filePath");
        String entityType = properties.getProperty("entityType");
        String sourceSystem = properties.getProperty("sourceSystem");
        String readOnly = properties.getProperty("readOnly");
        String threadCount = properties.getProperty("threadCount");
        String debugMode = properties.getProperty("debugMode");
        boolean isReadOnly = Boolean.parseBoolean(readOnly);
        boolean isDebugMode = Boolean.parseBoolean(debugMode);

        System.out.println("Tenant id:                  " + tenantId);
        System.out.println("Reltio API url:             " + reltioApiUrl);
        System.out.println("Reltio Auth url:            " + reltioAuthUrl);
        System.out.println("Auth user name:             " + authUserName);
        System.out.println("Auth user password:         " + authUserPassword);
        System.out.println("Restore Input file path:    " + inputFilePath);
        System.out.println("Entity type:                " + entityType);
        System.out.println("Source system:              " + sourceSystem);
        System.out.println("Thread count:               " + threadCount);
        System.out.println("Debug mode?:                " + debugMode);
        System.out.println("Read Only?:                 " + isReadOnly);
        System.out.println("Merge Reason:                 " + mergeReason);

        validInputParams(properties);

        BufferedReader fileReader = null;
        try {
            fileReader = new BufferedReader(new FileReader(inputFilePath));
        } catch (Exception ex) {
            System.out.printf("\nException in reading input file at :%s,Exiting...\n%n", inputFilePath);
            System.exit(1);
        }

        long recordCount = 0;
        try (Stream<String> lines = Files.lines(Paths.get(inputFilePath), Charset.defaultCharset())) {
            recordCount = lines.count();
        } catch (IOException e) {
            System.out.printf("\nException in reading input file at :%s,Exiting...\n%n", inputFilePath);
            System.exit(1);
        }
        int threads = 0;
        if (StringUtils.isBlank(threadCount)) {
            threads = (int) Math.ceil((double) recordCount / BATCH_SIZE);
        }

        threads = threads > MAX_THREADS ? MAX_THREADS : threads == 0 ? 1 : threads;
        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        final TokenGeneratorService tokenGeneratorService;
        ReltioAPIService reltioAPIService = null;

        try {
            tokenGeneratorService = new TokenGeneratorServiceImpl(authUserName, authUserPassword, reltioAuthUrl);
            reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
        } catch (APICallFailureException e) {
            System.out.println("\nException in getting auth token, Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());

        } catch (GenericException e) {
            System.out.println(e.getExceptionMessage());
        }

        System.out.printf("\nRunning restore entity task in %s threads...\n%n", threads);
        List<String> batch = Lists.newArrayList();
        AtomicLong processedCount = new AtomicLong(0);
        List<Future<List<EntityUnmergeStatus>>> futures = Lists.newArrayList();
        BufferedWriter writer = null;
        try {
            String line;
            LocalDateTime processStartTime = LocalDateTime.now();
            while ((line = fileReader.readLine()) != null) {
                batch.add(line);
                if (batch.size() >= BATCH_SIZE) {
                    UnmergeEntityExecutor batchTask = new UnmergeEntityExecutor(batch, entityType, reltioAPIService, reltioApiUrl, tenantId, sourceSystem, isReadOnly, isDebugMode, mergeReason);
                    futures.add(executorService.submit(batchTask));
                    batch.clear();
                }
            }
            if (!batch.isEmpty()) {
                UnmergeEntityExecutor batchTask = new UnmergeEntityExecutor(batch, entityType, reltioAPIService, reltioApiUrl, tenantId, sourceSystem, isReadOnly, isDebugMode, mergeReason);
                futures.add(executorService.submit(batchTask));
            }
            List<EntityUnmergeStatus> results = Collections.synchronizedList(new ArrayList<>());
            for (Future<List<EntityUnmergeStatus>> future : futures) {
                try {
                    while (!future.isDone()) {
                        Thread.sleep(1000);
                    }
                    List<EntityUnmergeStatus> batchResult = future.get();
                    processedCount.getAndAdd(batchResult.size());
                    System.out.printf("\nProcessed %s entities so far, Progress: %s %%\n%n", processedCount.get(), Math.round((double)processedCount.get()/recordCount * 100));
                    results.addAll(batchResult);
                } catch (InterruptedException | ExecutionException e) {
                    System.out.println("Unexpected exception: " + e.getMessage());
                }
            }
            long successCount = results.stream().filter(x -> {
                        EntityUnmergeStatus.Status status = x.getUnmergeStatus();
                        return status != null ? status.equals(EntityUnmergeStatus.Status.SUCCESSFUL) : false;
                    }).count();
            long failedCount = results.stream().filter(x -> {
                EntityUnmergeStatus.Status status = x.getUnmergeStatus();
                return status != null ? status.equals(EntityUnmergeStatus.Status.FAILED) : false;
            }).count();
            LocalDateTime completedTime = LocalDateTime.now();
            java.time.Duration duration = java.time.Duration.between(processStartTime, completedTime);
            System.out.printf("\nUnmerge entities task finished, time elapsed: %s seconds," + " processed entities: %s," + " unmerge success count : %s, unmerge failed count: %s\n%n", duration.getSeconds(), processedCount, successCount, failedCount);
            String timeColonPattern = "MM_dd_HH_mm_ss";
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(timeColonPattern);
            String outputFile = "unmerge_results_" + completedTime.format(dateTimeFormatter) + ".log";
            System.out.println("\nWriting unmerge results to file: "+ outputFile+" \n");
            writer = new BufferedWriter(new FileWriter(outputFile));
            final ObjectMapper mapper = new ObjectMapper();
            for (int i=0; i<results.size(); i++) {
                try {
                    writer.write(mapper.writeValueAsString(results.get(i)));
                    if (i != (results.size() -1)) writer.newLine();
                } catch (IOException ignored) {
                }
            }
            writer.flush();
        } catch (Exception ex) {
            System.out.println("Unexpected exception: "+ex.getMessage());
        } finally {
            fileReader.close();
            if (writer != null) {
                writer.close();
            }
            executorService.shutdown();
        }
    }

    private static void validInputParams(Properties properties) {
        for (String param : REQUIRED_PARAMS) {
            if (StringUtils.isBlank(properties.getProperty(param))) {
                System.out.printf("\nInput param: %s is not provided or blank. %s is required, Exiting now...\n%n", param, param);
                System.exit(1);
            }
        }
    }
}
