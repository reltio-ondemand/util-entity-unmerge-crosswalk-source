package com.reltio.scripts.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EntityUnmergeStatus {
    
    public enum Status {
        SUCCESSFUL, FAILED
    }
    
    @JsonProperty("entityUri")
    private String entityUri;
    @JsonProperty("sourceSystem")
    private String sourceSystem;
    @JsonProperty("unmergeStatus")
    private Status unmergeStatus;
    @JsonProperty("exceptionMessage")
    private String exceptionMessage;
    @JsonProperty("unmergeResponseUris")
    private List<String> unmergeResponseUris;
    @JsonProperty("timestamp")
    private Long timestamp;
    
    public EntityUnmergeStatus(String entityUri, String sourceSystem) {
        this.entityUri = entityUri;
        this.sourceSystem = sourceSystem;
    }
    
    public String getEntityUri() {
        return entityUri;
    }
    
    public void setEntityUri(String entityUri) {
        this.entityUri = entityUri;
    }
    
    public String getSourceSystem() {
        return sourceSystem;
    }
    
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
    
    public Status getUnmergeStatus() {
        return unmergeStatus;
    }
    
    public void setUnmergeStatus(Status unmergeStatus) {
        this.unmergeStatus = unmergeStatus;
    }
    
    public String getExceptionMessage() {
        return exceptionMessage;
    }
    
    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
    
    public List<String> getUnmergeResponseUris() {
        return unmergeResponseUris;
    }
    
    public void setUnmergeResponseUris(List<String> unmergeResponseUris) {
        this.unmergeResponseUris = unmergeResponseUris;
    }
    
    public Long getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        EntityUnmergeStatus that = (EntityUnmergeStatus) o;
        
        if (entityUri != null ? !entityUri.equals(that.entityUri) : that.entityUri != null) return false;
        if (sourceSystem != null ? !sourceSystem.equals(that.sourceSystem) : that.sourceSystem != null) return false;
        if (unmergeStatus != that.unmergeStatus) return false;
        if (exceptionMessage != null ? !exceptionMessage.equals(that.exceptionMessage) : that.exceptionMessage != null)
            return false;
        if (unmergeResponseUris != null ? !unmergeResponseUris.equals(that.unmergeResponseUris) : that.unmergeResponseUris != null)
            return false;
        return timestamp != null ? timestamp.equals(that.timestamp) : that.timestamp == null;
    }
    
    @Override
    public int hashCode() {
        int result = entityUri != null ? entityUri.hashCode() : 0;
        result = 31 * result + (sourceSystem != null ? sourceSystem.hashCode() : 0);
        result = 31 * result + (unmergeStatus != null ? unmergeStatus.hashCode() : 0);
        result = 31 * result + (exceptionMessage != null ? exceptionMessage.hashCode() : 0);
        result = 31 * result + (unmergeResponseUris != null ? unmergeResponseUris.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString() {
        return "EntityUnmergeStatus{" + "entityUri='" + entityUri + '\'' + ", sourceSystem='" + sourceSystem + '\'' + ", unmergeStatus=" + unmergeStatus + ", exceptionMessage='" + exceptionMessage + '\'' + ", unmergeResponseUris='" + unmergeResponseUris + '\'' + ", timestamp='" + timestamp + '\'' + '}';
    }
}
