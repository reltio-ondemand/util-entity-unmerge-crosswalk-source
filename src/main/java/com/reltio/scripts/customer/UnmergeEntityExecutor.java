package com.reltio.scripts.customer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class UnmergeEntityExecutor implements Callable<List<EntityUnmergeStatus>> {

    private final List<String> batch;
    private final ReltioAPIService apiService;
    private final boolean readOnly;
    private final ObjectMapper objectMapper;
    private final String sourceSystem;
    private final String entityType;
    private final String tenantUrl;
    private final boolean debugMode;
    public static final String DEFAULT_CROSSWALK_MERGE_REASON = "Merge by crosswalks";
    private String targetMergeReason = DEFAULT_CROSSWALK_MERGE_REASON;


    public UnmergeEntityExecutor(List<String> batch, String entityType, ReltioAPIService apiService, String apiUrl, String tenantId, String sourceSystem, boolean readOnly, boolean debugMode, String mergeReason) {
        this.batch = new ArrayList<>(batch);
        this.apiService = apiService;
        this.readOnly = readOnly;
        this.objectMapper = new ObjectMapper();
        this.sourceSystem = sourceSystem;
        this.entityType = entityType;
        this.tenantUrl = apiUrl +"/reltio/api/" + tenantId;
        this.debugMode = debugMode;
        if (mergeReason != null) {
            this.targetMergeReason = mergeReason;
        }
    }

    public UnmergeEntityExecutor(List<String> batch, String entityType, ReltioAPIService apiService, String apiUrl, String tenantId, String sourceSystem, boolean readOnly, boolean debugMode) {
        this(batch, entityType, apiService, apiUrl, tenantId, sourceSystem, readOnly, debugMode, null);
    }

    @Override
    public List<EntityUnmergeStatus> call() throws Exception {
        List<EntityUnmergeStatus> statusList = Lists.newArrayList();
        for (String line : batch) {
            String[] record = line.split(",");
            String entity = record[0];
            String source = record[1];
            EntityUnmergeStatus status = new EntityUnmergeStatus(entity, source);
            if (!source.equals(sourceSystem)){
                status.setExceptionMessage("Unmerge not supported for "+source+" source");
                status.setUnmergeStatus(EntityUnmergeStatus.Status.FAILED);
                statusList.add(status);
                continue;
            }

            try {
                System.out.printf("\nUnmerging entity: %s%n", entity);
                JsonNode crosswalks = getCrosswalkTree(entity);
                List<String> losers = getLoserUris(crosswalks, sourceSystem);
                if (debugMode) {
                    String loserList = losers.stream().collect(Collectors.joining(","));
                    System.out.printf("\nEntity: %s, SourceSystem : %s, Contributor Uris: %s%n",entity, source, loserList);
                }
                boolean success = false;
                if (!readOnly) {
                    for (String loser : losers) {
                        try {
                            String unmergeResponse = unMergeEntity(entity, loser);
                            success = true;
                            if (debugMode) {
                                JsonNode jsonResponse = objectMapper.readTree(unmergeResponse);
                                String currentEntity = jsonResponse.get("a").get("uri").asText();
                                String unmergedEntity = jsonResponse.get("b").get("uri").asText();
                                status.setTimestamp(System.currentTimeMillis());
                                List<String> uris = Lists.newArrayList();
                                uris.add(currentEntity);
                                uris.add(unmergedEntity);
                                status.setUnmergeResponseUris(uris);
                                status.setUnmergeStatus(EntityUnmergeStatus.Status.SUCCESSFUL);
                            }
                        } catch (Exception e) {
                            success = false;
                            status.setExceptionMessage(e.getMessage());
                            status.setUnmergeStatus(EntityUnmergeStatus.Status.FAILED);
                        }
                        if (success) {
                            updateEntity(loser);
                            continue;
                        }
                    }
                    if (success) {
                        updateEntity(entity);
                    }
                }
            } catch (Exception e) {
                String message = e instanceof ReltioAPICallFailureException ? ((ReltioAPICallFailureException) e).getErrorResponse() : (e instanceof GenericException ? ((GenericException) e).getExceptionMessage() : e.getMessage());
                status.setExceptionMessage(message);
                status.setUnmergeStatus(EntityUnmergeStatus.Status.FAILED);
            }
            statusList.add(status);
        }
        return statusList;
    }

    private String unMergeEntity(String entityUri, String loser) throws Exception {
        String unmergeApi = tenantUrl + "/" +entityUri + "/_unmerge?contributorURI="+loser;
        String response = null;
        String unmergeRes;
        try {
            response = apiService.post(unmergeApi, "");
            System.out.printf("\nEntity: %s unmerged successfully with: %s%n", entityUri, loser);
        } catch (ReltioAPICallFailureException e) {
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
            throw e;
        } catch (GenericException e) {
            throw e;
        }
        return response;
    }

    private String updateEntity(String entityUri) throws Exception {
        String updateApi = tenantUrl + "/entities?options=partialOverride";
        String response = null;
        try {
            ObjectMapper mapper = new ObjectMapper();

            // create three JSON objects
            ObjectNode entity = mapper.createObjectNode();
            entity.put("type", "configuration/entityTypes/"+entityType);

            ArrayNode values = mapper.createArrayNode();

            ArrayNode vvsArray = mapper.createArrayNode();
            ObjectNode vs = mapper.createObjectNode();
            vs.put("value", "ES");
            vvsArray.add(vs);

            ArrayNode vvdArray = mapper.createArrayNode();
            ObjectNode vd = mapper.createObjectNode();
            vd.put("value", System.currentTimeMillis());
            vvdArray.add(vd);

            ObjectNode attribute = mapper.createObjectNode();
            attribute.put("VendorVerificationStatus", vvsArray);
            attribute.put("VendorVerificationDate", vvdArray);

            ObjectNode value = mapper.createObjectNode();
            value.put("value", attribute);

            values.add(value);

            ObjectNode vendorVerification = mapper.createObjectNode();
            vendorVerification.put("VendorVerification",  values);


            entity.put("attributes", vendorVerification);

            ArrayNode crosswalks = mapper.createArrayNode();
            ObjectNode crosswalk = mapper.createObjectNode();
            crosswalk.put("type", "configuration/sources/Reltio");
            crosswalk.put("value", entityUri.split("/")[1]);
            crosswalks.add(crosswalk);
            entity.put("crosswalks", crosswalks);

            ArrayNode entities = mapper.createArrayNode();
            entities.add(entity);
            String body = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entities);
            response = apiService.post(updateApi, body);
            System.out.printf("\nEntity: %s attribute added successfully%n", entityUri);
        } catch (ReltioAPICallFailureException e) {
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
            throw e;
        } catch (GenericException e) {
            throw e;
        }
        return response;

    }

    private JsonNode getCrosswalkTree(String entityUri) throws Exception{
        String entityCrosswalks = tenantUrl+"/"+entityUri+"/_crosswalkTree";
        String response;
        JsonNode jsonNode = null;
        response = apiService.get(entityCrosswalks);
        if (response !=null) {
            jsonNode = objectMapper.readTree(response);
        }
        return jsonNode;
    }

    private List<String> getLoserUris(JsonNode crosswalkTree, String sourceSystem) {
        List<String> loserList = Lists.newArrayList();
        JsonNode merges = crosswalkTree.get("merges");
        if (merges != null && merges.getNodeType().equals(JsonNodeType.ARRAY)) {
            Iterator<JsonNode> iterator = merges.iterator();
            while (iterator.hasNext()) {
                JsonNode merge = iterator.next();
                String mergeReason = merge.get("mergeReason").textValue();
                if (mergeReason != null && mergeReason.equals(targetMergeReason)) {
                    JsonNode losers = merge.get("losers");
                    if (losers != null) {
                        Iterator<JsonNode> it = losers.iterator();
                        while (it.hasNext()) {
                            JsonNode jsonNode = it.next();
                            String loserUri = jsonNode.get("uri").textValue();
                            loserList.add(loserUri);
                        }
                    }
                }
            }
        }
        return loserList;
    }
}
